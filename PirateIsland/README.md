# Pirate Island
## Présentation
J'ai commencé à créer ce jeu dans l'optique de me former au moteur Unreal Engine. J'ai débuté au mois de juillet en suivant la formation d'openclassroom puis j'ai commencé à m'exercer à créer des petits jeux à la troisième personnes en suivant des tutoriels sur Youtube.

## Le Jeu
Je me suis donc lancé le défi de réaliser un petit jeu moi-même. Le but final étant qu'un personnage se déplace sur une île, puisse interargir avec des personnages lui proposant divers mini-jeux.

## Etat actuel
Pour l'instant, le personnage peut se déplacer avec les touches ZQSD, il peut sauter avec la barre espace et s'accroupir avec C. Il peut nager mais cela n'est pas encors fini(le personnage nage sous l'eau). Il peut interargir avec un personnage au centre du village (si vous parlez avec lui, vous allez rester bloqué car vous ne pouvez pas encore sélectionner de réponses). Il n'y a pas encore de menu, il faut donc appuyer sur alt+F4 pour fermer le jeu.
Je tiens à préciser que je n'ai pas fait les décors de l'île, je les ai téléchargée le store d'Epic Games, je me suis juste concentré sur le personnage, ses animation, ses déplacements et ses interactions.

## Avancée
J'essayerais de mettre une nouvelle version sur ce git à chaque grosse avancée du jeu. Les grosses avancées se feront essentiellement pendant les vacances scolaires étant données que je suis étudiant.